var margin = {top: 20, right: 20, bottom: 30, left: 40},
  width = 900 - margin.left - margin.right,
  height = 600 - margin.top - margin.bottom;

var svg = d3.select("body").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
var formatDate = d3.timeParse("%d-%b-%y");

var x = d3.scaleTime()
  .range([0, width]);

var y = d3.scaleLinear()
  .range([height, 0]);

var xAxis = d3.axisBottom()
  .scale(x);

var yAxis = d3.axisLeft()
  .scale(y);

var line = d3.line().curve(d3.curveCatmullRom.alpha(0.5))
  .x( (d) => { return x(d.date); })
  .y( (d) => { return y(d.close); });

var type = (d) => {
  d.date = formatDate(d.date);
  d.close = +d.close;
  return d;
};

d3.tsv("./data/LineChartData.tsv", type, (err, data) => {
  if (err) throw err;

  x.domain(d3.extent(data, (d) => { return d.date; }));
  y.domain(d3.extent(data, (d) => { return d.close; }));

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  ;

  svg.append("path")
    .datum(data)
    .attr("d", line)
    .attr("class","line")
    .attr("stroke", "blue")
    .attr("fill", "none")
    .attr("stroke-width", 2);

  svg.selectAll(".dot")
    .data(data.filter(function(d) { return d; }))
    .enter().append("circle")
    .attr("fill", "red")
    .attr("class", "dot")
    .attr("cx", line.x())
    .attr("cy", line.y())
    .attr("r", 3)
});

