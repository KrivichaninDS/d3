var dataset = [
  {
    name: "A",
    value: 5,
    color: "red"
  }, {
    name: "B",
    value: 10,
    color: "blue"
  },
  {
    name: "C",
    value: 15,
    color: "red"
  },
  {
    name: "D",
    value: 20,
    color: "blue"
  },
  {
    name: "E",
    value: 25,
    color: "red"
  }, {
    name: "F",
    value: 50,
    color: "red"
  }, {
    name: "G",
    value: 60,
    color: "blue"
  }, {
    name: "H",
    value: 30,
    color: "blue"
  }];

var margin = {top: 20, right: 20, bottom: 30, left: 40},
  width = 960 - margin.left - margin.right,
  height = 500 - margin.top - margin.bottom;

var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
var y = d3.scale.linear().range([height, 0]);
var z = d3.scale.scaleOrdinal()
  .range(["#001051", "#085100", "#D8120E", "#80056C", "#E8F000", "#41C887", "#BCEBD5", "#655F53"]);
var stack = d3.stack();

var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom");

var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")
  .ticks(10)
  .tickFormat(function(d) { return d+"%"});

var chart = d3.select(".chart")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

x.domain(dataset.map(function (d) { return d.name }))
y.domain([0, d3.max(dataset, function (d) { return d.value; })]);

var bar = chart.selectAll("g")
  .data(dataset)
  .enter().append("g")
  .attr("transform", function (d, i) {
    return "translate(" + x(d.name) + ",0)";
  })
  .attr("fill", function(d){ return d.color });

bar.append("rect")
  .attr("y", function (d) {
    return y(d.value);
  })
  .attr("height", function (d) {
    return height - y(d.value);
  })
  .attr("width", x.rangeBand());

bar.append("text")
  .attr("x", x.rangeBand() / 2)
  .attr("y", function (d) {
    return y(d.value) + 3;
  })
  .attr("dy", ".75em")
  .text(function (d) {
    return d.value;
  });

chart.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height + ")")
  .call(xAxis);

chart.append("g")
  .attr("class", "y axis")
  .call(yAxis)
  .append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text("Frequency");
