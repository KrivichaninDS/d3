var svg = d3.select("svg"),
  margin = {top: 20, right: 20, bottom: 30, left: 40},
  width = +svg.attr("width") - margin.left - margin.right,
  height = +svg.attr("height") - margin.top - margin.bottom,
  g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var x = d3.scaleBand()
  .rangeRound([0, width])
  .align(0.1);

var y = d3.scaleLinear()
  .rangeRound([height, 0]);

var z = d3.scaleOrdinal()
  .range(["#001051", "#085100", "#D8120E", "#80056C", "#E8F000", "#41C887", "#BCEBD5", "#655F53"]);

var startX = d3.scaleLinear()
  .domain([0, width])
  .range([0, 40]);

var cx = width - startX(width);

var stack = d3.stack();

d3.csv("./data/StackChartData.csv", type, function(error, data) {
  if (error) throw error;

  x.domain(data.map(function(d) { return d.Time; }));
  y.domain([0, d3.max(data, function(d) { return d.total; })]).nice();
  z.domain(data.columns.slice(1));

  g.selectAll(".serie")
    .data(stack.keys(data.columns.slice(1))(data))
    .enter().append("g")
    .attr("class", "serie")
    .attr("fill", function(d) { return z(d.key); })
    .selectAll("rect")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("x", function(d) { return x(d.data.Time); })
    .attr("y", function(d) { return y(d[1]); })
    .attr("height", function(d) { return y(d[0]) - y(d[1]); })
    .attr("width", x.bandwidth());

  g.append("g")
    .attr("class", "axis axis--x")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

  g.append("g")
    .attr("class", "axis axis--y")
    .call(d3.axisLeft(y).ticks(10, "s"))
    .append("text")
    .attr("x", 2)
    .attr("y", y(y.ticks(10).pop()))
    .attr("dy", "0.35em")
    .attr("text-anchor", "start")
    .attr("fill", "#000");

  var legend = g.selectAll(".legend")
    .data(data.columns.slice(1).reverse())
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", function(d, i) { return "translate(" + i * (-70) + ",-10)";})
    .style("font", "10px sans-serif");

  legend.append("circle")
    .attr("cx", cx - 50)
    .attr("cy", height / 50)
    .attr("r", 8)
    .attr("fill", z);

  legend.append("text")
    .attr("x", cx)
    .attr("y", 10)
    .attr("dy", ".35em")
    .attr("text-anchor", "end")
    .text(function(d) { return d; });
});

function type(d, i, columns) {
  for (i = 1, t = 0; i < columns.length; ++i) t += d[columns[i]] = +d[columns[i]];
  d.total = t;
  return d;
}

/*Styles
  .bar {
  fill: steelblue;
}

.axis path {
  display: none;
}*/
